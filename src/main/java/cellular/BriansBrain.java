package cellular;

import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Random;

public class BriansBrain implements CellAutomaton{
    /**
     * The grid of cells
     */
    IGrid currentGeneration;

    /**
     *
     * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
     * provided size
     *
     * @param height The height of the grid of cells
     * @param width  The width of the grid of cells
     */
    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                } else {
                    currentGeneration.set(row, col, CellState.DEAD);
                }
            }
        }
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public CellState getCellState(int row, int col) {
        return currentGeneration.get(row, col);
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
        for(int i = 0; i < nextGeneration.numRows(); i++){
            for(int j = 0; j < nextGeneration.numColumns(); j++){
                CellState NextCell = getNextCell(i, j);
                nextGeneration.set(i, j, NextCell);
            }
        }
        currentGeneration = nextGeneration;
    }

    @Override
    public CellState getNextCell(int row, int col)  {
        CellState State = currentGeneration.get(row, col);
        int NumNeighbours = countNeighbors(row, col, CellState.ALIVE);
        if(State == CellState.ALIVE){
            return CellState.DYING;
        }
        else if(State == CellState.DEAD && NumNeighbours == 2){
            return CellState.ALIVE;
        }
        return CellState.DEAD;
    }

    /**
     * Calculates the number of neighbors having a given CellState of a cell on
     * position (row, col) on the board
     *
     * Note that a cell has 8 neighbors in total, of which any number between 0 and
     * 8 can be the given CellState. The exception are cells along the boarders of
     * the board: these cells have anywhere between 3 neighbors (in the case of a
     * corner-cell) and 5 neighbors in total.
     *
     * @param x     the x-position of the cell
     * @param y     the y-position of the cell
     * @param state the Cellstate we want to count occurences of.
     * @return the number of neighbors with given state
     */
    private int countNeighbors(int row, int col, CellState state) {
        int numNeighbours = 0;
        for (int i = row-1; i <= row+1; i++) {
            for (int j = col-1; j <= col+1; j++) {
                if(!(i == row && j == col) && (i >= 0 && i < currentGeneration.numRows())
                        && (j >= 0 && j < currentGeneration.numColumns()) && currentGeneration.get(i, j) == state){
                    numNeighbours++;
                }
            }
        }
        return numNeighbours;
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
}
