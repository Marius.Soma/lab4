package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int columns;
    private int rows;
    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.columns = columns;
        this.grid = new CellState[rows][columns];
        
        for(int i = 0; i<this.rows; i++) {
            for (int n = 0; n<this.columns; n++){
                grid[i][n] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (isOB(row, column)) {
            throw new IndexOutOfBoundsException("this index does not exist in your grid");
        } else {
            this.grid[row][column] = element;
        }
    }

    @Override
    public CellState get(int row, int column) {
        if (isOB(row, column)) {
            throw new IndexOutOfBoundsException("this index does not exist in your grid");
        } else {
            return this.grid[row][column];
        }
    }

    @Override
    public IGrid copy() {
        CellGrid gridCopy = new CellGrid(numRows(), numColumns(), null);
        for (int i = 0; i < numRows(); i++){
            for (int n = 0; n < numColumns(); n++) {
                gridCopy.set(i, n, get(i, n));
            } 
        }
        return gridCopy;
    }
    
    public boolean isOB(int row, int column){
        if (row > numRows() || column > numColumns() || row<0 || column<0) {
            return true;
        }else {
            return false;
        }
    }   
}
